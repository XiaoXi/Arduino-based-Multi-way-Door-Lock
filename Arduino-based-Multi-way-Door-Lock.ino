// 载入函数库
#include <Servo.h> // 伺服舵机
#include <Wire.h>
#include "LiquidCrystal_I2C.h" // LCD 显示屏
#include "IRremote.h" // 红外遥控
#include "rfid.h" // RFID

// 配置
#define configDoorContinuousOnTime 3000 // 整型，开门 操作时 门锁持续开启时间（ms）
#define configServoTurnTheAngle 83 // 整型，开门 操作时 舵机转动的角度（°）
#define configBuzzerEnable 1 // 布尔型，是否启用 蜂鸣提示音
#define configBatteryShowEnable 0 // 布尔型，是否启用 电量显示模块
#define configRfidEnable 1 // 布尔型，是否启用 RFID 模块
#define config433RemoteEnable 0 // 布尔型，是否启用 433 遥控模块
#define configIrRemoteCodeServeRun 0xFF906F // 十六进制，开门 红外遥控编码（0xFF906F 为“EQ”按键）
#define configIrRemoteCodeBatteryShow 0xFFC23D // 十六进制，电量显示 红外遥控编码（0xFFC23D 为“播放”按键）

// 引脚位置绑定
#define componentPinServoDoorLock 10 // 舵机
#define componentPinBuzzer 8 // 蜂鸣器
#define componentPinNpn 9 // NPN 三极管（用于舵机电源控制）
#define componentPinRelay 103 // 继电器（用于电量显示模块电源控制）
#define modulePinIrRemote 11 // 红外遥控接收器模块
#define modulePin433RemoteA 105 // 433 遥控接收器模块 A 引脚
#define modulePin433RemoteB 106 // 433 遥控接收器模块 B 引脚
#define modulePin433RemoteC 107 // 未使用，433 遥控接收器模块 C 引脚
#define modulePin433RemoteD 108 // 未使用，433 遥控接收器模块 D 引脚

// 主功能逻辑计时器变量
unsigned long timeServoRun = millis(), timeServoRunRecord = 0, timeBatteryShow = millis(), timeBatteryShowRecord = 0;

// 433 遥控模块按键消抖逻辑变量
//boolean module433RemoteAOld = 1; // 按键消抖 历史记录
boolean module433RemoteAAccess = 0, module433RemoteBAccess = 0; // 按键消抖 通过状态

// RFID 模块变量
uchar moduleRfidSeriesNumber[5];

// 其他变量
int valueServoDoorLockAngle = 0; // 舵机角度

// 舵机 初始化
Servo servoDoorLock;

// 红外遥控 模块初始化
IRrecv irrecv(modulePinIrRemote);
decode_results results;

// LCD 模块初始化
LiquidCrystal_I2C lcd(0x27, 16, 2);

// RFID 模块初始化
RFID rfid;

void setup() {
	// 引脚功能绑定
	pinMode(componentPinBuzzer, OUTPUT);
	pinMode(componentPinNpn, OUTPUT);
	pinMode(componentPinRelay, OUTPUT);
	pinMode(modulePinIrRemote, INPUT); // 非必要，上方已指定为 红外遥控 引脚
	pinMode(modulePin433RemoteA, INPUT_PULLUP);
	pinMode(modulePin433RemoteB, INPUT_PULLUP);
	pinMode(modulePin433RemoteC, INPUT_PULLUP);
	pinMode(modulePin433RemoteD, INPUT_PULLUP);

	// 舵机 绑定
	servoDoorLock.attach(componentPinServoDoorLock);

	// 舵机 复位
	digitalWrite(componentPinNpn, HIGH);
	servoDoorLock.write(valueServoDoorLockAngle);
	delay(1000);
	digitalWrite(componentPinNpn, LOW);

	// 红外遥控 启用
	irrecv.enableIRIn();

	// LCD 显示屏 启用
	lcd.init();

	// RFID 启用
	rfid.begin(6, 3, 4, 5, 2, 7);//rfid.begin(IRQ_PIN,SCK_PIN,MOSI_PIN,MISO_PIN,NSS_PIN,RST_PIN)
	delay(100);
	rfid.init();

	// 波特率
	Serial.begin(9600);
}

void loop() {
	// 红外遥控 数据处理
	if (irrecv.decode(&results)) {
		Serial.print("irCode: ");
		Serial.print(results.value, HEX);
		Serial.print(", bits: ");
		Serial.println(results.bits);
		irrecv.resume();
	}

	// 载入函数
	if (config433RemoteEnable == 1) {
		func433Remote(); // 433 遥控模块运行逻辑函数
	}
	if (configRfidEnable == 1) {
		funcRfid(); // RFID 模块运行逻辑函数
	}
	funcServoRun(); // 舵机运行逻辑函数
	if (configBatteryShowEnable == 1) {
		funcBatteryShow(); // 电量显示模块运行逻辑函数
	}
}

void func433Remote() {
	// 433 遥控 数据读取
	boolean module433RemoteA = digitalRead(modulePin433RemoteA);
	boolean module433RemoteB = digitalRead(modulePin433RemoteB);
	boolean module433RemoteC = digitalRead(modulePin433RemoteC);
	boolean module433RemoteD = digitalRead(modulePin433RemoteD);

	// 全按键消抖逻辑
	/*
	if (module433RemoteA == 1) { // 判断按键电位，“1”为按键被触发
		if (module433RemoteA != module433RemoteAOld) { // 判断当前电位与上次 loop 时的电位是否一致，如不一致则执行
			module433RemoteAAccess = 1; // 设置按键操作为“生效”
			module433RemoteAOld = 1;
		}
	}
	else {
		module433RemoteAOld = 0;
	}*/
	if (module433RemoteA == 1) { // 无需消抖
		module433RemoteAAccess = 1;
	}

	if (module433RemoteB == 1) { // 无需消抖
		module433RemoteBAccess = 1;
	}

	// 433 遥控 入口 A
	if (module433RemoteAAccess == 1) {
		digitalWrite(componentPinNpn, HIGH); // NPN 导通（舵机 接入供电）
		timeServoRunRecord = millis();
		module433RemoteAAccess = 0;
	}

	// 433 遥控 入口 B
	if (module433RemoteBAccess == 1) {
		digitalWrite(componentPinRelay, LOW); // 继电器接通（电量显示模块 接入供电）
		timeBatteryShowRecord = millis();
		module433RemoteBAccess = 0;
	}
}

void funcRfid() {
	// RFID 数据处理
	uchar rfidStatus;
	uchar rfidStr[MAX_LEN];
	rfidStatus = rfid.request(PICC_REQIDL, rfidStr);
	if (rfidStatus != MI_OK) {
		return;
	}
	rfid.showCardType(rfidStr); // 显示卡片信息
	rfidStatus = rfid.anticoll(rfidStr);

	if (rfidStatus == MI_OK) {
		Serial.print("The RFID card's number is: ");
		memcpy(moduleRfidSeriesNumber, rfidStr, 5);
		rfid.showCardID(moduleRfidSeriesNumber); // 显示卡片 ID
		Serial.println();

		// 检查卡片 ID 为 2F075380
		uchar* id = moduleRfidSeriesNumber;
		if (id[0] == 0x2F && id[1] == 0x07 && id[2] == 0x53 && id[3] == 0x80) {
			timeServoRunRecord = millis();
		}
	}

	rfid.halt(); // 命令卡片进入睡眠模式
}

void funcServoRun() {
	// 红外遥控入口
	if (results.value == configIrRemoteCodeServeRun) {
		digitalWrite(componentPinNpn, HIGH); // NPN 导通（舵机 接入供电）
		timeServoRunRecord = millis();
		results.value = 0;
	}

	// 主功能逻辑
	timeServoRun = millis() - timeServoRunRecord;
	if (timeServoRun < 60 || (timeServoRun >= 120 && timeServoRun < 180) || (timeServoRun >= configDoorContinuousOnTime - 100 && timeServoRun < configDoorContinuousOnTime)) { // 通用 蜂鸣器 触发
		digitalWrite(componentPinBuzzer, configBuzzerEnable);
		servoDoorLock.write(valueServoDoorLockAngle + configServoTurnTheAngle); // 舵机 旋转 configServoTurnTheAngle 度
	}
	else if ((timeServoRun >= 60 && timeServoRun < 120) || (timeServoRun >= 180 && timeServoRun < 240)) { // 通用 蜂鸣器 低电平
		digitalWrite(componentPinBuzzer, LOW);
	}
	else if (timeServoRun >= configDoorContinuousOnTime && timeServoRun < configDoorContinuousOnTime + 2000) { // 舵机 等待 configDoorContinuousOnTime 后复位
		servoDoorLock.write(valueServoDoorLockAngle);
		digitalWrite(componentPinBuzzer, LOW);
	}
	else if (timeServoRun >= configDoorContinuousOnTime + 2000) { // NPN 断开（舵机 切断供电）
		digitalWrite(componentPinNpn, LOW);
	}
}

void funcBatteryShow() {
	// 红外遥控入口
	if (results.value == configIrRemoteCodeBatteryShow) {
		digitalWrite(componentPinRelay, LOW); // 继电器接通（电量显示模块 接入供电）
		timeBatteryShowRecord = millis();
		results.value = 0;
	}

	// 主功能逻辑
	timeBatteryShow = millis() - timeBatteryShowRecord;
	if (timeBatteryShow >= 3000) { // 继电器断开（电量显示模块 切断供电）
		digitalWrite(componentPinRelay, HIGH);
	}
}
