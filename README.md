# Arduino-based Multi-way Door Lock

⭐ 基于 Arduino 的多控制方式伺服舵机门锁 ⭐

## 🤔 这是什么

这是一个基于 Arduino 实现的伺服舵机门锁，原理简单，支持多种控制方式。

## ✨ 功能实现

- ⚙️ 伺服舵机运转（支持 MG995 等 180° 舵机）
- 🔊 开锁 / 闭锁蜂鸣提示音
- 📇 RFID 卡支持
- 🔴 红外遥控
- 🎛️ 433 遥控（固定码）
- 🔋 电量显示
- 🔒 伺服舵机待机自动断电
- 🛠️ 更多功能开发中...

## 📜 开源许可

基于 [GNU Lesser General Public License v3.0](https://choosealicense.com/licenses/lgpl-3.0/) 许可进行开源。
